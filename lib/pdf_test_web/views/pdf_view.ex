defmodule PdfTestWeb.PdfView do
  use PdfTestWeb, :view
  alias PdfTest.PdfGenerator

  @newline_regex ~r/[\n|\r\n|\\\\|\r]/
  @replace_regex ~r/[\n|\r\n|\r]/

  def render("invoice.pdf", assigns) do
    latex = render("invoice.tex", assigns)
    {:ok, pdf} = PdfGenerator.latex_to_pdf(latex)
    pdf
  end

  def format_table_cell(text) when is_binary(text) do
    case Regex.match?(@newline_regex, text) do
      true ->
        text
        |> String.replace(@replace_regex, "\\\\\\\\")
        |> makecell

      false ->
        text
    end
  end

  def makecell(text) do
    ["\\makecell[tl]{", text, 125]
  end
end
