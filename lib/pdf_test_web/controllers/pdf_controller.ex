defmodule PdfTestWeb.PdfController do
  use PdfTestWeb, :controller

  @seller %{
    name: "Jan Kowalski",
    address: "ul. Piłsudskiego 69,\n 00-000 Warszawa\n Poland",
    nip: "106-00-00-062",
    phone: "666-666-666"
  }

  @client %{
    name: "Grzegorz Brzęczyszczykiewicz",
    address: "ul. Stepana Bandery 2137,\n 00-000 Ostrowiec Świętokrzyski\n Ukraine",
    nip: "106-00-00-062",
    phone: "666-213-742"
  }

  def index(conn, _params) do
    render(conn, "invoice.pdf", %{client: @client, seller: @seller})
  end
end
