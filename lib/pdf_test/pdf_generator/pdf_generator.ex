defmodule PdfTest.PdfGenerator do
  import Phoenix.View, only: [render_to_iodata: 3]

  @xelatexoptions ["-halt-on-error", "-interaction=nonstopmode"]

  def render_to_pdf(view_module, view_template, assigns) do
    render_to_iodata(view_module, view_template, assigns)
    |> latex_to_pdf()
  end

  def latex_to_pdf(latex) do
    base_filename = generate_basename()
    File.mkdir_p(".temp")

    dir = Path.expand(".temp")

    File.write(base_filename <> ".tex", latex)

    case compile(base_filename, dir) do
      {_, 0} ->
        {:ok, pdf} = File.read(base_filename <> ".pdf")
        clean_up(base_filename)
        {:ok, pdf}

      {error, 1} ->
        clean_up(base_filename)
        {:error, error}
    end
  end

  defp compile(filename, dir) do
    System.cmd("xelatex", @xelatexoptions ++ [filename], cd: dir)
  end

  defp clean_up(base_filename) do
    (base_filename <> ".*")
    |> Path.wildcard()
    |> Enum.each(&File.rm/1)
  end

  defp generate_basename do
    filename =
      :crypto.strong_rand_bytes(8)
      |> Base.encode16(case: :lower)

    Path.expand(".temp") <> "/" <> filename
  end
end
