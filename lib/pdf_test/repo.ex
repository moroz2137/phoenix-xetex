defmodule PdfTest.Repo do
  use Ecto.Repo,
    otp_app: :pdf_test,
    adapter: Ecto.Adapters.Postgres
end
